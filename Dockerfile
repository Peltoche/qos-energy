FROM golang:1.6-onbuild


RUN go test db/*
CMD ["go", "run", "main.go", "import", "ressources/dataset.csv"]
