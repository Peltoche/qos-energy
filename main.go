package main

import (
	"encoding/csv"
	"errors"
	"log"
	"os"
	"time"

	_ "github.com/go-sql-driver/mysql"
	_ "github.com/mattn/go-sqlite3"
	"github.com/urfave/cli"
	"gitlab.com/Peltoche/qos-energy/db"
)

func importCSV(c *cli.Context, connection *db.Db) error {
	// Retrieve the path from the arguments.
	path := c.Args().Get(0)
	if len(path) == 0 {
		return errors.New("missing file path")
	}

	// Load the file from the file.
	file, err := os.Open(path)
	if err != nil {
		return err
	}
	defer file.Close()

	// Create a reader and load all the data.
	//
	// TODO: If the goal is reading very big files it will be best to change the ReadAll function for a stream of
	// producer system.
	reader := csv.NewReader(file)
	records, err := reader.ReadAll()
	if err != nil {
		return err
	}

	// Iter on each line and save it in database.
	for _, value := range records[1:] {
		// Parse date
		date, err := time.Parse("01/02/2006", value[0])
		if err != nil {
			return err
		}

		// Create model.
		row := db.MeetingModel{
			Date: date,
			Name: value[1],
		}

		// Save it.
		if _, err := connection.Meeting.Save(row); err != nil {
			return err
		}
	}

	return nil
}

func main() {
	connection, err := db.NewFromEnv()
	if err != nil {
		log.Fatal(err)
	}

	app := cli.NewApp()
	app.Name = "QosTest"
	app.Usage = "Some usage here"

	app.Commands = []cli.Command{
		cli.Command{
			Name:  "import",
			Usage: "Import a new csv file",
			Action: func(c *cli.Context) error {
				return importCSV(c, connection)
			},
		},
	}

	app.Run(os.Args)
}
