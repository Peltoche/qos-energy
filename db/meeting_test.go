package db

import (
	"log"
	"os"
	"testing"
	"time"

	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/Peltoche/qos-energy/db"
)

func connect() *db.Db {
	// Force the environment as TEST.
	os.Setenv("ENVIRONMENT", "TEST")

	connection, err := db.NewFromEnv()
	if err != nil {
		log.Fatal(err)
	}

	return connection
}

func TestCreate(t *testing.T) {
	connection := connect()

	model := db.MeetingModel{
		Date: time.Now(),
		Name: "Foo Bar",
	}

	id, err := connection.Meeting.Save(model)
	if err != nil {
		t.Errorf("Error during creation: %+v", err)
	}

	res, err := connection.Meeting.FindOne(id)
	if err != nil {
		t.Errorf("Error during retrieving: %+v", err)
	}

	if res.Name != model.Name {
		t.Errorf("Not the same name: %+v", err)
	}

	log.Println("date res: ", res.Date)
	log.Println("date model: ", res.Date)
	if !res.Date.Equal(model.Date) {
		t.Errorf("Not the same date.")
	}
}
