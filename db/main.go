/*
Package db handle all the actions with the database.

As db permit a generic interface with the database no sql request should
be outside this package.
*/
package db

import (
	"database/sql"
	"errors"
	"fmt"
	"log"
	"os"
	"strings"
)

const (
	defaultDevDbPath = "./db.sqlite"
)

// Db represent the interface between the modules and the database.
type Db struct {
	client  *sql.DB
	Meeting MeetingTable
}

// NewFromEnv return a new Db from the env variables.
func NewFromEnv() (*Db, error) {
	env := strings.ToUpper(os.Getenv("ENVIRONMENT"))
	var client *sql.DB
	var err error

	log.Println("Try to load <" + env + "> database")

	// Load the appropriate driver
	switch env {

	// Load the DEV database.
	case "DEV":
		// Retrieve env values.
		path := os.Getenv("DEV_DB_PATH")
		if len(path) == 0 {
			path = defaultDevDbPath
		}

		// Create a sqlite3 client.
		client, err = sql.Open("sqlite3", path)
		if err != nil {
			return nil, err
		}

		// Load the TEST database.
	case "TEST":
		// Create a sqlite3 client.
		client, err = sql.Open("sqlite3", ":memory:")
		if err != nil {
			return nil, err
		}

	// Load the PRODUCTION database.
	case "PRODUCTION":
		// Retrieve env values.
		user := os.Getenv("DB_USER")
		password := os.Getenv("DB_PASSWORD")
		address := os.Getenv("DB_ADDRESS")
		database := os.Getenv("DB_DATABASE")

		// Create a mysql client.
		client, err = sql.Open("mysql", user+":"+password+"@"+address+"/"+database+
			"?parseTime=true")
		if err != nil {
			log.Fatal(fmt.Errorf("Fail to connect to sql: %v", err))
		}

	default:
		return nil, errors.New("missing or invalid ENVIRONMENT value")
	}

	// Instanciate tables.
	meeting, err := NewMeetingTable(client)
	if err != nil {
		return nil, err
	}

	// Create object.
	connection := &Db{
		client:  client,
		Meeting: meeting,
	}

	return connection, nil
}
