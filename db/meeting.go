package db

import (
	"database/sql"
	"fmt"
	"log"
	"time"
)

// MeetingTable represent the interface between the modules and the "Meeting" table.
type MeetingTable struct {
	client *sql.DB
}

// MeetingModel represent the data inside the Meeting Table.
type MeetingModel struct {
	ID   int
	Date time.Time
	Name string
}

// NewMeetingTable instanciate a new Meeting interface.
//
// Create an struct Interface and create the Table
func NewMeetingTable(client *sql.DB) (MeetingTable, error) {
	meeting := MeetingTable{
		client: client,
	}

	_, err := meeting.client.Exec(`
	CREATE TABLE IF NOT EXISTS Meeting (
		id	  INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
		date	DATETIME NOT NULL,
		name	VARCHAR(40) NOT NULL
	)
	`)
	if err != nil {
		return meeting, fmt.Errorf("error creating table Meeting : %v", err)
	}

	return meeting, err
}

// Save a meeting inside the database.
//
// Return the id of the created element.
func (table MeetingTable) Save(meeting MeetingModel) (int64, error) {
	res, err := table.client.Exec(`
	INSERT INTO Meeting (date, name)
	VALUES (?, ?)
	`,
		meeting.Date,
		meeting.Name)
	if err != nil {
		return 0, fmt.Errorf("error saving meeting: %v", err)
	}

	id, err := res.LastInsertId()
	if err != nil {
		return 0, fmt.Errorf("error retrieving meeting id: %v", err)
	}

	log.Println("Save id: ", id)

	return id, nil
}

// FindOne meeting inside the database.
//
// Return `nil` if no row.
func (table MeetingTable) FindOne(id int64) (*MeetingModel, error) {
	var row MeetingModel

	err := table.client.QueryRow(`
	SELECT
		id, date, name
	FROM
		Meeting
	WHERE
		id=?
	`,
		id).Scan(
		&row.ID,
		&row.Date,
		&row.Name,
	)
	if err == sql.ErrNoRows {
		return nil, nil
	}

	if err != nil {
		return nil, fmt.Errorf("error FindOne Meeting: %v", err)
	}

	return &row, nil
}
