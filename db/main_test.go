package db

import (
	"os"
	"testing"

	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/Peltoche/qos-energy/db"
)

func TestConnectTest(t *testing.T) {
	// Force the environment as TEST.
	os.Setenv("ENVIRONMENT", "TEST")

	_, err := db.NewFromEnv()
	if err != nil {
		t.Errorf("Fail to connect: %+v", err)
	}
}

func TestConnectDev(t *testing.T) {
	dbPath := "/tmp/db_test_name_not_used"

	// Force the environment as DEV.
	os.Setenv("ENVIRONMENT", "DEV")
	os.Setenv("DEV_DB_PATH", dbPath)

	_, err := db.NewFromEnv()
	if err != nil {
		t.Errorf("Fail to connect: %+v", err)
	}

	err = os.Remove(dbPath)
	if err != nil {
		t.Errorf("Fail to remove db: %+v", err)
	}
}

func TestConnectDevWithTableCreated(t *testing.T) {
	dbPath := "/tmp/db_test_name_not_used"

	// Force the environment as DEV.
	os.Setenv("ENVIRONMENT", "DEV")
	os.Setenv("DEV_DB_PATH", dbPath)

	_, err := db.NewFromEnv()
	if err != nil {
		t.Errorf("Fail to connect: %+v", err)
	}

	_, err = db.NewFromEnv()
	if err != nil {
		t.Errorf("Fail to connect: %+v", err)
	}

	err = os.Remove(dbPath)
	if err != nil {
		t.Errorf("Fail to remove db: %+v", err)
	}
}
